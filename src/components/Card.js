import React from "react";
import moment from "moment";
import "../styles/card.scss";
import { BiCommentDetail } from "react-icons/bi";
import Clamp from "react-multiline-clamp";
import { IoEyeSharp } from "react-icons/io5";
import { Link } from "react-router-dom";

const Card = ({
	id,
	img,
	title,
	desc,
	comments,
	views,
	slug,
	user,
	createdAt,
}) => {
	const timeago = moment(createdAt).fromNow();
	return (
		<div className="card">
			<Link to={`/blog/${id}`} className="card__img">
				<img src={img} alt={slug} />
			</Link>
			<div className="card__info">
				<div className="card__info-container">
					<Clamp withTooltip lines={3}>
						<Link to={`/blog/${id}`} className="card__info--title">
							{title}
						</Link>
					</Clamp>
					<Clamp withTooltip lines={3}>
						<span className="card__info--desc">{desc}</span>
					</Clamp>
				</div>
				<div className="card__info--footer">
					<div className="profile">
						<p className="profile__name">
							{user.sFirstName} {user.sLastName}
						</p>
						<p className="profile__timestamp">{timeago}</p>
					</div>
					<div className="counter">
						<div className="counter__comments">
							<BiCommentDetail />
							<span className="counter__comments--number">{comments}</span>
						</div>
						<div className="counter__views">
							<IoEyeSharp />
							<span className="counter__views--number">{views}</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Card;
