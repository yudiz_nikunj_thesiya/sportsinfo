import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import { BsArrowLeft } from "react-icons/bs";
import data from "../data/sports-info.json";
import "../styles/blog.scss";
import moment from "moment";
import { BiCommentDetail } from "react-icons/bi";
import Clamp from "react-multiline-clamp";
import { IoEyeSharp } from "react-icons/io5";

const Blog = () => {
	const [blog, setBlog] = useState([]);
	const { id } = useParams();
	// console.log(id);

	useEffect(() => {
		const blogInfo = data.data.filter((curr) => curr._id === id);
		setBlog(blogInfo);
	}, []);

	const timeago = (timestamp) => {
		return moment(timestamp).fromNow();
	};

	return blog.map((info) => (
		<div className="blog" key={info._id}>
			<div className="blog__img">
				<img src={info.sImage} alt={info.sSlug} />
				<div className="blog__img--title-container">
					<Link to="/" className="icon">
						<BsArrowLeft />
					</Link>
					<div className="title">{info.sTitle}</div>
					<div className="footer">
						<div className="profile">
							<p className="profile__name">
								{info.iId.sFirstName} {info.iId.sLastName}
							</p>
							<p className="profile__timestamp">{timeago(info.dCreatedAt)}</p>
						</div>
						<div className="counter">
							<div className="counter__comments">
								<BiCommentDetail />
								<span className="counter__comments--number">
									{info.nCommentsCount}
								</span>
							</div>
							<div className="counter__views">
								<IoEyeSharp />
								<span className="counter__views--number">
									{info.nViewCounts}
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="blog__details">
				<span className="blog__details--desc">{info.sDescription}</span>
			</div>
		</div>
	));
};

export default Blog;
