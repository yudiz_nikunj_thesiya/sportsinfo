import React, { useEffect, useState } from "react";
import Card from "../components/Card";
import "../styles/home.scss";
import data from "../data/sports-info.json";

const Home = () => {
	const [info, setInfo] = useState([]);
	useEffect(() => {
		setInfo(data.data);
	}, []);
	return (
		<div className="home">
			<div className="home__cards">
				{info.map((curr) => (
					<Card
						key={curr._id}
						id={curr._id}
						img={curr.sImage}
						title={curr.sTitle}
						desc={curr.sDescription}
						comments={curr.nCommentsCount}
						user={curr.iId}
						slug={curr.sSlug}
						views={curr.nViewCounts}
						createdAt={curr.dCreatedAt}
					/>
				))}
			</div>
		</div>
	);
};

export default Home;
